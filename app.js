var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// modelos

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect("mongodb://localhost/juan");

var user_schema = new Schema({
	nombre: {type:String},
	apellido: {type:String},
	cedula: {type:String},
password: {type:String,minlength:[4,"pass muy corta"]},
email: {type: String,required: "el correo es obligatorio"}
});


var User = mongoose.model("User",user_schema);
// modelos



app.get('/',function(req,res,next){
	res.render('index')
});
app.get('/singup',function(req,res,next){
	res.render('signup')
});


app.get('/login',function(req,res,next){
	res.render('login')
});

app.post('/json',function(req,res,next){
	User.find({'local.user' : User}, function( err, user){
			res.send({User : user})
	})

});

app.post("/users",function(req,res){
var user = new User({nombre: req.body.nombre,
									 apellido: req.body.apellido,
									 cedula: req.body.cedula,
									 email: req.body.email,
	                 password: req.body.password,
	                 password_confirmation: req.body.password_confirmation
	                 });

user.save().then(function(us){
	res.send("guardamos el usuario");
}, function(err){
	if(err){
		console.log(String(err));
		res.send("no pudimos guardar el usuario");
	       }
     });
res.redirect('login')
});

app.get('/administrador', function(req,res){
	res.render('administrador')
});


app.post("/administrador",function(req,res){


	User.find({email: req.body.email,password: req.body.password, },function(err,user){
		if(!err && req.body.email == 'dani.21vch@gmail.com' && req.body.password == '2500' ) {
			 	 res.redirect('/singup');
			 	}

		else res.redirect('/');

        });
});




module.exports = app;
